FROM kalilinux/kali-rolling

RUN apt-get update \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y \
	gvm \
	openvas
