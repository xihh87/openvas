Contenedor de OpenVAS para análisis de vulnerabilidades.

Instrucciones de uso
--------------------

Para utilizar este servicio,
[configurar podman](https://wiki.archlinux.org/title/Podman ) y
ejecutar:

```
$ podman-compose up -d
$ podman-compose exec openvas bash
# gvmd --user=admin --new-password=${NEW_PASSWORD}
# install -d -o _gvm /run/gvm
# gvm-start
```

Consultar el servicio en <https://localhost:9392>.

En máquina virtual
------------------

```
# apt-get update
# apt-get install -y openvas
# gvm-setup
```

## Casos de uso

## Cómo contribuir

## Referencias

[Especificación de `compose.yml`](https://docs.docker.com/compose/compose-file/compose-file-v3/ ).

[Cómo hacer funcionar `systemd` en contenedores](https://joshua.haase.mx/techno/systemd-docker/ ).

[La opción más popular de openvas](https://github.com/mikesplain/openvas-docker/blob/master/docker-compose.yml )
se llama [mikesplain/openvas](https://hub.docker.com/r/mikesplain/openvas )
y [al parecer instala muchísimas dependencias](https://github.com/mikesplain/openvas-docker/blob/master/9/Dockerfile ).
